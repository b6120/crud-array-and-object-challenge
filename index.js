let toDoList = [];

//contoh object di array//
let activity = {
    name: "olahraga",
    tanggal: "6-1-2022",
    status: "not finish",
    id: 1
}

function addActivity(activity) {
    toDoList.push(activity);
}

function deleteActivity(activity) {
    console.log("kegiatan yang akan didelet:", activity);
  let newToDolist = toDoList.filter(function(item){
    return item.name !== activity
  })
  toDoList = newToDolist
  console.log("list sekarang: ", toDoList)
}

function getActivityList () {
    console.log("kegiatan yang ada:",toDoList)
}

function updateStatus (activity, state){
  console.log("activity yang akan di update: ", activity);
  toDoList.forEach(function(item){
    if (item.name == activity){
      item.status = state;
    }
  })
  console.log(`status ${activity} sekarang menjadi`,state)
  console.log(toDoList)
}

addActivity ({
    name: "berenang",
    date: "6-1-2022",
    status: "not finish"
});

addActivity ({
    name: "memanah",
    date: "5-1-2022",
    status: "finish"
})

getActivityList();

deleteActivity("memanah");

addActivity ({
  name: "berkuda",
  date: "5-1-2022",
  status: "finish"
})

getActivityList();

updateStatus("renang", "finish");